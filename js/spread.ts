let a = [2,3,4];
console.log([ 1, a, 5 ]); // [ 1, [ 2, 3, 4 ], 5 ]
console.log([ 1, ...a, 5 ]); // [ 1, 2, 3, 4 , 5 ]

function bar(y: number, z: number) { console.log
( "//spread", y, z ); }
bar( ...[1,2] );
//spread 1 2

let clonedArray = [...a];

function foo(y: number, ...z: number[]) {
  console.log("//rest ", y, z);
}

foo(1, 2, 3, 4, 5);
//rest 1 [2,3,4,5]