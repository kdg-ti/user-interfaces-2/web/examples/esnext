
let bottle
console.log(bottle) // undefined
console.log(bottle??"bottle is empty") // bottle is empty
// commenting out the next line will abort the program
//console.log(bottle.size) // Uncaught TypeError: bottle is undefined.
console.log(bottle?.size) //undefined
console.log(bottle?.size??"1 liter") //bottle is empty1 liter
let drink
drink ??=  "water" // assign if drink is null / undefined
console.log(drink) // water
drink ??=  "wine"
console.log(drink) // water
drink =  ""
drink ||=  "wine" // assign if drink is falsy (empty string, 0, false…)
console.log(drink) // rum
