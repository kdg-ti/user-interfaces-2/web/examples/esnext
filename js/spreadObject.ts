const herProfile = {
  name: "Elisabeth",
  age: 36
};
const herAddress = {
  name: "Liza",
  address: {
	zip: 2000,
	community: "Antwerp"
  }
};
const all = {...herProfile, ...herAddress};
console.log("after spread:",all);